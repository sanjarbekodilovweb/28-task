import Navbar from "./components/Navbar";
import Section from "./components/Section";
import Footer from "./components/Footer";
import Product from "./components/products/Product";
import Texnika from "./components/products/Texnika";
import React, {Component} from 'react'
import Cart from "./components/cart/Cart";
import {BrowserRouter as Router, Route, Switch,} from "react-router-dom";
import Profile from "./components/profile/Profile";
import API from "./components/products/Api";


class App extends Component {

    constructor() {
        super();
        this.state = {
            tex: [
                {
                    id: 1, name: 'Samsung Galaxy A51', brand: 'Samsung',
                    price: 250, imgURL: '/images/Tel-samsung.jpg'
                },
                {
                    id: 2, name: 'Iphone 12 Pro Max', brand: 'Apple',
                    price: 1200, imgURL: '/images/Tel-apple.png'
                },
                {
                    id: 3, name: 'Artel Premium Black', brand: 'Artel',
                    price: 120, imgURL: '/images/Tel-artel.png'
                },
                {
                    id: 4, name: 'LG Q60 Niebieski', brand: 'LG',
                    price: 120, imgURL: '/images/Tel-lg.jpg'
                },
                {
                    id: 5, name: 'FHD Smart TV N5300 Series 5', brand: 'Samsung',
                    price: 500, imgURL: '/images/Television-samsung.jpg'
                },
                {
                    id: 6, name: 'Sony KD-55XD7005', brand: 'Sony',
                    price: 400, imgURL: '/images/TV-sony.jpg'
                },
                {
                    id: 7, name: 'Artel 65AU90GS Smart TV', brand: 'Artel',
                    price: 260, imgURL: '/images/Television-artel.jpg'
                },
                {
                    id: 8, name: 'LG Smart TV', brand: 'LG',
                    price: 360, imgURL: '/images/TV-lg.jpg'
                },
                {
                    id: 9, name: 'Iwatch', brand: 'Apple',
                    price: 12, imgURL: '/images/iwatch.jpg'
                },
                {
                    id: 10, name: 'Iwatch 4 series ', brand: 'Apple',
                    price: 9.95, imgURL: '/images/iwatch-4-series.jpg'
                },
                {
                    id: 11, name: 'Iwatch 6 series ', brand: 'Apple',
                    price: 9.95, imgURL: '/images/iwatch-6-series.jpg'
                },
                {
                    id: 12, name: 'Iwatch 3 series', brand: 'Apple',
                    price: 9, imgURL: '/images/iwatch-3-series.jpg'
                },

            ],
            cardInfo: [
                {
                    id: 1,
                    name: 'Lee',
                    balance: 20000,
                    number: '1234 5678 9123 4567',
                    expiry: '12/34',
                    cvc: '5554',

                },
                {
                    id: 2,
                    name: 'Kim',
                    balance: 20000,
                    number: '1234 5678 9123 4568',
                    expiry: '12/35',
                    cvc: '5555',

                },
                {
                    id: 3,
                    name: 'Pack',
                    balance: 30000,
                    number: '1234 5678 9123 4569',
                    expiry: '12/36',
                    cvc: '5556',


                },
            ],
            orderInfo:
                {
                    id: 123456,
                    firstName: "Jackie",
                    lastName: "Chan",
                    totalPrice: 0,
                },
            orderedProducts: [

            ],
            history: [

            ],
            products: [
                {
                    id: 1, name: 'T-shirt', brand: 'Nike', size: 'XL',
                    price: 25, imgURL: '/images/t-shirt-nike.jpg'
                },
                {
                    id: 2, name: 'T-shirt', brand: 'Reebok', size: 'XXL',
                    price: 20, imgURL: '/images/t-shirt-reebok.jpg'
                },
                {
                    id: 3, name: 'Pants', brand: 'Reebok', size: '33',
                    price: 30, imgURL: '/images/pants-reebok.jpg'
                },
                {
                    id: 4, name: 'Pants', brand: 'Nika', size: '31',
                    price: 30, imgURL: '/images/pants-nike.jpg'
                },
                {
                    id: 5, name: 'Cap', brand: 'Nika', size: 'Standart',
                    price: 30, imgURL: '/images/cap-nike.jpg'
                },
                {
                    id: 6, name: 'Cap', brand: 'Adidas', size: 'Standart',
                    price: 32, imgURL: '/images/cap-adidas.jpg'
                },
                {
                    id: 7, name: 'Snickers', brand: 'Adidas', size: '42',
                    price: 125, imgURL: '/images/snickers-adidas.jpg'
                },
                {
                    id: 8, name: 'Snickers', brand: 'Nika', size: '40',
                    price: 140, imgURL: '/images/snickers-nike.jpg'
                },
            ],
            tempCount: 1,
            tempPrice: 0,
        }

    }

    changeCount = (item, status) => {
        if (status === true) {
            this.setState({
                tempCount: this.state.tempCount + 1,
                tempPrice: (this.state.tempCount + 1) * item.price
            })
        } else if (this.state.tempCount >= 1 && !status) {
            this.setState({
                tempCount: this.state.tempCount - 1,
                tempPrice: (this.state.tempCount - 1) * item.price
            })
        } else if (status === 'cart-plus') {
            const {orderedProducts} = this.state

            let objectId = this.state.orderedProducts.findIndex(obj => obj.id === item)
            orderedProducts[objectId].totalCount += 1
            orderedProducts[objectId].totalPrice = orderedProducts[objectId].totalCount * orderedProducts[objectId].price
            this.setState({
                orderedProducts: orderedProducts
            })

            this.calculateAllPrice()

        } else if (status === 'cart-minus') {
            const {orderedProducts} = this.state

            let objectId = this.state.orderedProducts.findIndex(obj => obj.id === item)

            if (orderedProducts[objectId].totalCount >= 1) {
                orderedProducts[objectId].totalCount -= 1
            }

            orderedProducts[objectId].totalPrice = orderedProducts[objectId].totalCount * orderedProducts[objectId].price
            this.setState({
                orderedProducts: orderedProducts
            })
            this.calculateAllPrice()
        }
    }

    updateData = (modal) => {
        if (!modal) {
            this.setState({
                tempCount: 1,
                tempPrice: 0
            })
        }
    }

    addToCart = (item) => {
        const {tempCount, orderedProducts, orderInfo,} = this.state
        if (tempCount >= 1) {
            item.orderId = orderInfo.id
            item.totalCount = tempCount
            item.totalPrice = tempCount * item.price
            orderedProducts.push(item)
        }
    }
    addToHistory = () => {
        let {tempCount, orderedProducts, history,} = this.state

        if (tempCount >= 1) {
            history=[...orderedProducts,...history]
            this.setState({
                history
            })

        }

    }


    calculateAllPrice = () => {
        let priceCounter = 0

        const {orderedProducts, orderInfo} = this.state

        orderedProducts.map(item => {
            priceCounter = priceCounter + item.totalPrice
        })
        orderInfo.totalPrice = priceCounter
        this.setState({
            orderInfo: orderInfo
        })

    }

    deleteProductFromCart = (id) => {

        const {orderedProducts} = this.state
        let objectId = orderedProducts.findIndex(obj => obj.id === id)
        if (id === null) {
            orderedProducts.splice(0)
        } else {
            orderedProducts.splice(objectId, 1)
        }
        this.setState({
            orderedProducts: orderedProducts
        })

        this.calculateAllPrice()
    }


    render() {

        const {products, tempCount, tempPrice, tex, orderedProducts, history, cardInfo, orderInfo} = this.state


        return (
            <Router>
                <Navbar orderedProducts={orderedProducts} orderInfo={orderInfo}
                        allProductsPrice={orderInfo.totalPrice}
                        calculateAllPrice={this.calculateAllPrice}
                />
                <Switch>
                    <Route exact path="/">
                        <Section/>
                    </Route>

                    <Route exact path="/api">
                        <API/>
                    </Route>
                    <Route path="/cart" render={() => (
                        <Cart orderedProducts={orderedProducts} orderInfo={orderInfo}
                              allProductsPrice={orderInfo.totalPrice}
                              deleteProductFromCart={(id) => this.deleteProductFromCart(id)}
                              changeCount={(item, status) => this.changeCount(item, status)}
                              cardInfo={cardInfo} addToHistory={(item) => this.addToHistory(item)}
                        />
                    )}/>

                    <Route path="/por" render={() => (
                        <Profile
                                 deleteProductFromCart={(id) => this.deleteProductFromCart(id)}
                                 history={history}
                        />
                    )}/>
                    <Route path="/products" render={() => (
                        <Product tempCount={tempCount} tempPrice={tempPrice} data={products}
                                 changeCount={(item, status,) => this.changeCount(item, status,)}
                                 updateData={(modal) => this.updateData(modal)}
                                 addToCart={(item) => this.addToCart(item)}
                        />
                    )}/>

                    <Route path="/technology" render={() => (
                        <Texnika tempCount={tempCount} tempPrice={tempPrice} mydata={tex}
                                 changeCount={(item, status) => this.changeCount(item, status)}
                                 updateData={(modal) => this.updateData(modal)}
                                 addToCart={(item) => this.addToCart(item)}
                        />
                    )}/>

                </Switch>

                <Footer/>
            </Router>
        );
    }
}
export default App;

