import React, {useState} from "react";
import {Button, ButtonGroup, Modal, ModalBody, ModalFooter, ModalHeader,} from "reactstrap";
import PaymentForm from "../pay/creditcart";


export default function Cart(props) {

    const {
        orderedProducts,
        allProductsPrice,
        cardInfo,
        addToHistory,
        changeCount,
        deleteProductFromCart
    } = props


    const [modal, setModal] = useState(false)

    const toggle = () => {

        setModal(!modal)
    }

    return (

        <div className="container my-5">
            <div className="d-flex justify-content-between">
                <h1 className="mb-5">My cart</h1>
                <div className="d-flex align-items-center">
                    <h4>All products price:
                        <span className="badge badge-primary">{allProductsPrice}$</span></h4>
                    <Button color="warning" className="ml-3" onClick={toggle}
                    >Checkout</Button>
                    <Button color="danger" className="ml-3"
                            onClick={() => deleteProductFromCart(null)}>DeleteAll</Button>
                </div>
            </div>

            <div className="row">
                {orderedProducts.length ? orderedProducts.map((item, index) => (
                        <div key={index} className="col-lg-6">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="card border-0">
                                        <img className="w-100" src={item.imgURL} alt="product"/>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="card border-0">
                                        <h5>{item.name}, {item.brand}</h5>
                                        <h5>Size: {item.size}</h5>
                                        <h5>Price: <span className="badge badge-danger">{item.price}$</span></h5>
                                        <h5>Total count: <span className="badge badge-warning">{item.totalCount}</span></h5>
                                        <h5>Total price: <span className="badge badge-danger">{item.totalPrice}$</span></h5>
                                    </div>
                                    <div className="card border-0 my-4">
                                        <ButtonGroup>
                                            <Button onClick={() => changeCount(item.id, 'cart-minus')}>-</Button>
                                            <Button outline color="secondary" disabled>{item.totalCount}</Button>
                                            <Button onClick={() => changeCount(item.id, 'cart-plus')}>+</Button>
                                        </ButtonGroup>
                                    </div>
                                    <div className="card border-0">
                                        <Button color="danger"
                                                onClick={() => deleteProductFromCart(item.id)}>Remove</Button>
                                    </div>
                                </div>
                                <div className="col-lg-12">
                                    <hr/>
                                </div>
                            </div>
                        </div>
                    )) :
                    <div className="col-lg-2 offset-5">
                        <div className="card border-0">
                            <h2>No data</h2>
                        </div>
                    </div>
                }
            </div>

            <Modal isOpen={modal} toggle={toggle} backdrop="static">
                <ModalHeader toggle={toggle}>Modal title</ModalHeader>
                <ModalBody>

                    <PaymentForm toggle={toggle} allProductsPrice={allProductsPrice} orderedProducts={orderedProducts}
                                 deleteProductFromCart={deleteProductFromCart} addToHistory={addToHistory} cardInfo={cardInfo}/>

                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}
