import React, {useState, useEffect} from 'react';
import axios from "axios";
import {Collapse, Button, CardBody, Card, Spinner, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {toast} from "react-toastify";
import {AvForm, AvField} from 'availity-reactstrap-validation';


let className = 'container'

function Section() {

    const [loader, setLoader] = useState(false);
    const [posts, setPosts] = useState([]);
    const [error, setError] = useState(null);
    const [tempId, setTempId] = useState(null);
    const [comments, setComments] = useState([]);
    const [modal, setModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [editPost, setEditPost] = useState(null);
    const [deletePostId, setDeletePostId] = useState(null);


    const toggle = id => {

        setTempId(tempId === id ? null : id)

        axios.get('https://jsonplaceholder.typicode.com/comments?postId=' + id).then(response => {
            setComments(response.data)
        }).catch(error => {
            setError(error)
        })

    }

    useEffect(() => {
        fetchPosts()
    }, [])

    const fetchPosts = () => {

        setLoader(true)
        axios.get('https://jsonplaceholder.typicode.com/posts').then(response => {
            setPosts(response.data)
            setLoader(false)
        }).catch(error => {
            setError(error)
            setLoader(false)
        })
    }

    const toggleModal = () => {
        setModal(!modal)
        setEditPost(null)
    }

    const handleValidSubmit = (events, values) => {
        toggleModal()
        if (editPost) {
            axios.put('https://jsonplaceholder.typicode.com/posts/' + editPost.id, {values})
                .then(response => {
                    toast.success('Edited successfully.')
                }).catch(error => {
                toast.error('Error.')
            })
        } else {
            axios.post('https://jsonplaceholder.typicode.com/posts', values)
                .then(response => {
                    console.log(response)
                    toast.success("Post added")
                }).catch(error => {
                toast.error("Error.")
            })
        }

        fetchPosts()
    }

    const editOrDeletePost = (idOrItem, status) => {
        if (status === 'edit') {
            toggleModal()
            setEditPost(idOrItem)
        } else {
            toggleDeleteModal(idOrItem)
        }

    }

    const toggleDeleteModal = (id) => {
        setDeleteModal(!deleteModal)
        setDeletePostId(id)

    }

    const agreeToDelete = (answer) => {
        if (answer==='yes'){
            axios.delete('https://jsonplaceholder.typicode.com/posts/' +deletePostId)
                .then(response=>{
                    toast.warning('Deleted successfully')
                }).catch(error=>{
                    toast.error('Error')
            })
            setDeleteModal(false)
        }else {
            setDeleteModal(false)
        }
    }


    return (
        <section className="section-1 py-lg-5">
            {error ? <React.StrictMode>{toast.error('Serverda xatolik')}</React.StrictMode> : null}
            <div className={className}>
                <div className="d-flex justify-content-between my-lg-3">
                    <h2>Posts</h2>
                    <Button onClick={toggleModal} className="btn btn-danger">
                        Add new post
                    </Button>
                </div>
                <div className="row">

                    {loader ?
                        <div className="col-lg-2 offset-5 text-center">
                            <Spinner color="dark"/>
                        </div>
                        :
                        posts.length && posts.map((item, index) => (
                            <div key={index} className="col-lg-12 mb-lg-3">
                                <div className="card">
                                    <div className="card-body">
                                        <h4>{index + 1}. {item.title}</h4>
                                        <h5>{item.body}</h5>
                                    </div>
                                    <div className="card-footer">
                                        <div className="d-flex justify-content-between">
                                            <Button color="danger" onClick={() => toggle(item.id)}
                                                    style={{marginBottom: '1rem'}}>
                                                Comments
                                            </Button>
                                            <div>
                                                <Button onClick={() => editOrDeletePost(item, 'edit')} color="warning"
                                                        className="mr-2">
                                                    Edit
                                                </Button>
                                                <Button onClick={() => editOrDeletePost(item.id, 'delete')} color="danger">
                                                    Delete
                                                </Button>
                                            </div>

                                        </div>
                                        <Collapse isOpen={tempId === item.id}>
                                            <Card>
                                                {comments.length && comments.map((item, index) => (
                                                    <CardBody key={index}>
                                                        <h4>{index + 1}. {item.name}</h4>
                                                        <h5>{item.email}</h5>
                                                        <p>{item.body}</p>
                                                        <hr/>
                                                    </CardBody>
                                                ))}
                                            </Card>
                                        </Collapse>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>

            <Modal isOpen={modal} toggle={toggleModal} backdrop="static">
                <ModalHeader toggle={toggleModal}>Modal title</ModalHeader>
                <AvForm onValidSubmit={handleValidSubmit}>
                    <ModalBody>

                        <AvField value={editPost ? editPost.title : ''} name="title" label="Title" type="text"
                                 required/>
                        <AvField value={editPost ? editPost.body : ''} name="body" label="Body" type="textarea"
                                 required/>
                        <AvField value={editPost ? editPost.userId : ''} name="userId" label="User Id" type="number"
                                 required/>
                    </ModalBody>

                    <ModalFooter>
                        <Button color="primary" type="submit">Submit</Button>{' '}
                        <Button color="secondary" onClick={toggleModal}>Cancel</Button>
                    </ModalFooter>
                </AvForm>
            </Modal>

            <Modal isOpen={deleteModal} toggle={() => toggleDeleteModal(null)} backdrop="static">
                <ModalHeader toggle={() => toggleDeleteModal(null)}>Modal title</ModalHeader>

                <ModalBody>

                    Do you want delete?
                </ModalBody>

                <ModalFooter>
                    <Button color="primary" type="submit" onClick={() => agreeToDelete('yes')}>Yes</Button>{' '}
                    <Button color="secondary" onClick={() => agreeToDelete('no')}>No</Button>
                </ModalFooter>

            </Modal>

        </section>
    )
}

export default Section